from ubuntu:22.04 as build-gowin

arg DEBIAN_FRONTEND=noninteractive
env LANG=en_US.utf8
run apt-get update -qq \
 && apt-get -y install --no-install-recommends \
    libboost-all-dev libomp5-14 libeigen3-dev libomp-dev \
    python3-pip git ca-certificates curl \
    cmake build-essential \
 && apt-get autoclean && apt-get clean && apt-get -y autoremove \
 && rm -rf /var/lib/apt/lists/*

run curl -L -o nextpnr.tgz "https://github.com/YosysHQ/nextpnr/archive/refs/tags/nextpnr-0.6.tar.gz" \
 && mkdir -p /tmp/nextpnr \
 && tar -xvf nextpnr.tgz -C /tmp/nextpnr --strip-components=1 \
 && rm nextpnr.tgz \
 && mkdir /tmp/nextpnr/build/

run pip install apycula

run curl -o gowin.tgz "http://cdn.gowinsemi.com.cn/Gowin_V1.9.8_linux.tar.gz" && \
    tar -xvf gowin.tgz && \
    rm gowin.tgz

run cd /tmp/nextpnr/build \
 && cmake .. \
    -DARCH=gowin \
 && make -j$(nproc) \
 && make DESTDIR=/opt/nextpnr install


from ubuntu:22.04 as build-yosys

arg DEBIAN_FRONTEND=noninteractive
env LANG=en_US.utf8
run apt-get update -qq \
 && apt-get -y install --no-install-recommends \
    bison flex gawk gcc pkg-config zlib1g-dev clang git make \
    libffi-dev libreadline-dev tcl-dev graphviz xdot \
    git ca-certificates curl \
    cmake build-essential \
 && apt-get autoclean && apt-get clean && apt-get -y autoremove \
 && rm -rf /var/lib/apt/lists/*

run curl -L -o yosys.tgz "https://github.com/YosysHQ/yosys/archive/refs/tags/yosys-0.29.tar.gz" \
 && mkdir -p /tmp/yosys \
 && tar -xvf yosys.tgz -C /tmp/yosys --strip-components=1 \
 && rm yosys.tgz

run cd /tmp/yosys \
 && make -j $(nproc) \
 && make DESTDIR=/opt/yosys install 


from ubuntu:22.04

arg DEBIAN_FRONTEND=noninteractive
env LANG=en_US.utf8
run apt-get update -qq \
 && apt-get install -y locales \
 && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 \
 && apt-get install --no-install-recommends -y \
        python3-pip sudo file vim less procps tree tar tmux git-lfs bash-completion \
        tcl libboost-filesystem1.74.0 libboost-program-options1.74.0 libboost-thread1.74.0 \
 && apt-get autoclean && apt-get clean && apt-get -y autoremove \
 && rm -rf /var/lib/apt/lists/*

run pip install apycula
copy --from=build-gowin /opt/nextpnr /
copy --from=build-yosys /opt/yosys /

arg CODER_VERSION=4.11.0
add https://github.com/coder/code-server/releases/download/v${CODER_VERSION}/code-server_${CODER_VERSION}_amd64.deb /tmp/code-server.deb
run dpkg -i /tmp/code-server.deb && rm /tmp/code-server.deb

run useradd coder \
      --create-home \
      --shell=/bin/bash \
      --uid=1000 \
      --user-group && \
    echo "coder ALL=(ALL) NOPASSWD:ALL" >>/etc/sudoers.d/nopasswd

user coder
workdir /home/coder

entrypoint [ "/usr/bin/code-server", "--auth", "none", "--port", "13337", "--bind-addr", "0.0.0.0" ]
