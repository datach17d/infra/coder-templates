#!/usr/lib/code-server/lib/node

var exec = require('child_process').exec;

function moment (data) {
    return data;
}

exec("sed -n '/var allToolsInformation = {/,/}\;/p' $(grep -r -l \"var allToolsInformation\" /home/coder/.local/share/code-server/extensions)",
    function (error, stdout, stderr) {
        if (error !== null) {
             console.log('exec error: ' + error);
        }
        else {
            var gocodeClose = ""
            const semver = {
                coerce(data) {
                    return data;
                },
                parse(data) {
                    return data;
                }
            }

            const semver2 = semver;

            semver.coerce("test")
            eval(stdout);
            
            for (const [key, value] of Object.entries(allToolsInformation)) {
                if(!value.replacedByGopls && (value.isImportant || key == "dlv")) {
                    console.log("Installing " + value.importPath)
                    exec("/usr/local/go/bin/go install " + value.importPath + "@latest",
                        function (error, stdout, stderr) {
                            if (error !== null) {
                                console.log('exec error: ' + error);
                           }
                        }
                    );
                } else {
                    console.log('skipping ' + value.importPath)
                }
            }
        }
    });
