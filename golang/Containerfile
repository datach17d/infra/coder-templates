from ubuntu:22.04

arg DEBIAN_FRONTEND=noninteractive
env LANG=en_US.utf8
run apt-get update && \
    apt-get install -y locales && \
    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 && \
    apt-get install --no-install-recommends -y \
        git-lfs git wget sudo bash-completion ca-certificates make jq curl

arg GOLANG_VERSION=1.23.4
arg GOLANG_HASH=6924efde5de86fe277676e929dc9917d466efa02fb934197bc2eba35d5680971
add https://go.dev/dl/go${GOLANG_VERSION}.linux-amd64.tar.gz /tmp/golang.tar.gz
env PATH=$PATH:/usr/local/go/bin
run echo ${GOLANG_HASH} /tmp/golang.tag.gz | sha256sum
run tar -xvf /tmp/golang.tar.gz -C /usr/local && rm /tmp/golang.tar.gz

arg NODE_VERSION=22.12.0
arg NODE_PACKAGE=node-v$NODE_VERSION-linux-x64
arg NODE_HOME=/opt/$NODE_PACKAGE
env NODE_PATH $NODE_HOME/lib/node_modules
env PATH $NODE_HOME/bin:$PATH
run curl https://nodejs.org/dist/v$NODE_VERSION/$NODE_PACKAGE.tar.gz | tar -xzC /opt/


arg CODER_VERSION=4.96.2
add https://github.com/coder/code-server/releases/download/v${CODER_VERSION}/code-server_${CODER_VERSION}_amd64.deb /tmp/code-server.deb
run dpkg -i /tmp/code-server.deb && rm /tmp/code-server.deb

run useradd coder \
      --create-home \
      --shell=/bin/bash \
      --uid=1000 \
      --user-group && \
    echo "coder ALL=(ALL) NOPASSWD:ALL" >>/etc/sudoers.d/nopasswd

user coder
workdir /home/coder/workspace

env HOME=/home/coder
env PATH=$PATH:/usr/local/go/bin:$HOME/go/bin

run curl -vv -L -o /tmp/golang.vsix https://open-vsx.org/api/golang/Go/0.44.0/file/golang.Go-0.44.0.vsix
run code-server --verbose --install-extension /tmp/golang.vsix
#run code-server --verbose --install-extension golang.go

copy install_tools.js .
run ./install_tools.js && rm -f install_tools.js && sudo rm -rf $HOME/go/pkg

entrypoint [ "/usr/bin/code-server", "--auth", "none", "--port", "13337", "--bind-addr", "0.0.0.0" ]
