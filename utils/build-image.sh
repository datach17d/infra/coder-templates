#!/bin/sh -x

BUILD_TOOL=${BUILD_TOOL:-docker}
CONTEXT_DIR=${CONTEXT_DIR:-${PWD}}
TAG=${TAG:-}
IMAGE_NAME=${IMAGE_NAME:-$(basename ${CONTEXT_DIR})}

if [ -z "${TAG}" ]; then
  if [ -f "${CONTEXT_DIR}/main.tf" ]; then
    TAG=$(grep "image.*${IMAGE_NAME}" ${CONTEXT_DIR}/main.tf | sed "s|.*:\(.*\)\"|\1|g")
  fi
fi

BUILD_ARGS=""

if [ "${BUILD_TOOL}" = "/kaniko/executor" ]; then
  BUILD_ARGS="--cache --context ${CONTEXT_DIR} --dockerfile ${CONTEXT_DIR}/Containerfile --destination ${REGISTRY}${IMAGE_NAME}:${TAG} ${BUILD_ARGS}"
fi

if [ "${BUILD_TOOL}" = "docker" ]; then
  BUILD_ARGS="build --tag ${REGISTRY}${IMAGE_NAME}:${TAG} --file ${CONTEXT_DIR}/Containerfile ${BUILD_ARGS} ${CONTEXT_DIR}"
fi

"${BUILD_TOOL}" ${BUILD_ARGS}
