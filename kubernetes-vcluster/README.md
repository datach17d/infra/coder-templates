---
name: Develop in Kubernetes
description: Get started with Kubernetes development.
tags: [cloud, kubernetes]
---

# Kubernetes-vcluster vcluster + vscode

This creates a vcluster instance and provisions credentials in vscode to access the vcluster.

The following tools are installed in vscode to manage the vcluster:
 - kubectl
 - kustomize
 - helm
 - k9s

