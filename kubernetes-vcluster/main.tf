terraform {
  required_providers {
    coder = {
      source  = "coder/coder"
      version = "0.6.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.12.1"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.7.1"
    }
  }
}

locals {
  release_name = "vcluster-${data.coder_workspace.me.owner}-${data.coder_workspace.me.name}"
  release_namespace = "${local.release_name}"
}


resource "kubernetes_namespace" "vcluster" {
  metadata {
    name = "${local.release_namespace}"
  }
}

resource "helm_release" "vcluster-dev" {
  name       = "${local.release_name}-dev"
  namespace  = "${local.release_namespace}"
  timeout = 120

  depends_on = [
    kubernetes_namespace.vcluster
  ]

  repository = "https://charts.loft.sh"
  chart      = "vcluster"
  version    = "0.14.0"

  values = [<<EOF
syncer:
  extraArgs:
    - --out-kube-config-server=https://${local.release_name}-dev.${local.release_namespace}
  kubeConfigContextName: "dev"

mapServices:
  fromVirtual:
    - from: ingress-nginx/ingress-nginx-controller
      to: ingress-nginx-dev-controller

sync:
  persistentvolumes:
    enabled: true
  hoststorageclasses:
    enabled: true

storage:
  persistence: false

#init:
#  helm:
#    - chart:
#        name: ingress-nginx 
#        repo: https://kubernetes.github.io/ingress-nginx
#        version: "4.4.0"
#      release:
#        name: ingress-nginx
#        namespace: ingress-nginx
#      values: |-
#        controller:
#          service:
#            type: ClusterIP

EOF
  ]
}

resource "kubernetes_ingress_v1" "vcluster-ingress-dev" {
  metadata {
    name = "vcluster-ingress-dev"
    namespace = local.release_namespace
    annotations = {
      "nginx.ingress.kubernetes.io/rewrite-target" = "/$2"
    }
  }

  depends_on = [
    helm_release.vcluster-dev
  ]

  spec {
    ingress_class_name = "nginx"
    rule {
      # FIXME: remove this hardcoded domain and get it from host cluster?
      host = "whome.sneak.codes"
      http {
        path {
          path = "/${local.release_name}-dev(/|$)(.*)"
          backend {
            service {
              name = "ingress-nginx-dev-controller"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}

resource "helm_release" "vcluster-prod" {
  name       = "${local.release_name}-prod"
  namespace  = "${local.release_namespace}"
  timeout = 120

  depends_on = [
    kubernetes_namespace.vcluster
  ]

  repository = "https://charts.loft.sh"
  chart      = "vcluster"
  version    = "0.14.0"

  values = [<<EOF
syncer:
  extraArgs:
    - --out-kube-config-server=https://${local.release_name}-prod.${local.release_namespace}
  kubeConfigContextName: "prod"

mapServices:
  fromVirtual:
    - from: ingress-nginx/ingress-nginx-controller
      to: ingress-nginx-prod-controller

sync:
  persistentvolumes:
    enabled: true
  hoststorageclasses:
    enabled: true

storage:
  persistence: false

#init:
#  helm:
#    - chart:
#        name: ingress-nginx 
#        repo: https://kubernetes.github.io/ingress-nginx
#        version: "4.4.0"
#      release:
#        name: ingress-nginx
#        namespace: ingress-nginx
#      values: |-
#        controller:
#          service:
#            type: ClusterIP

EOF
  ]
}

resource "kubernetes_ingress_v1" "vcluster-ingress-prod" {
  metadata {
    name = "vcluster-ingress-prod"
    namespace = local.release_namespace
    annotations = {
      "nginx.ingress.kubernetes.io/rewrite-target" = "/$2"
    }
  }

  depends_on = [
    helm_release.vcluster-prod
  ]

  spec {
    ingress_class_name = "nginx"
    rule {
      # FIXME: remove this hardcoded domain and get it from host cluster?
      host = "whome.sneak.codes"
      http {
        path {
          path = "/${local.release_name}-prod(/|$)(.*)"
          backend {
            service {
              name = "ingress-nginx-prod-controller"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}

resource "helm_release" "vcluster-control" {
  name       = "${local.release_name}-control"
  namespace  = "${local.release_namespace}"
  timeout = 120

  depends_on = [
    kubernetes_namespace.vcluster,
    helm_release.vcluster-dev,
    helm_release.vcluster-prod
  ]

  repository = "https://charts.loft.sh"
  chart      = "vcluster"
  version    = "0.14.0"

  values = [<<EOF
syncer:
  extraArgs:
    - --out-kube-config-server=https://${local.release_name}-control.${local.release_namespace}
  kubeConfigContextName: "control"

mapServices:
  fromVirtual:
    - from: ingress-nginx/ingress-nginx-controller
      to: ingress-nginx-control-controller
  fromHost:
    - from: ${local.release_name}-dev
      to: ${local.release_namespace}/${local.release_name}-dev
    - from: ${local.release_name}-prod
      to: ${local.release_namespace}/${local.release_name}-prod


sync:
  persistentvolumes:
    enabled: true
  hoststorageclasses:
    enabled: true

storage:
  persistence: false

init:
  helm:
    - chart:
        name: ingress-nginx 
        repo: https://kubernetes.github.io/ingress-nginx
        version: "4.4.0"
      release:
        name: ingress-nginx
        namespace: ingress-nginx
      values: |-
        controller:
          service:
            type: ClusterIP

EOF
  ]
}

resource "kubernetes_ingress_v1" "vcluster-ingress-control" {
  metadata {
    name = "vcluster-ingress-control"
    namespace = local.release_namespace
    annotations = {
      "nginx.ingress.kubernetes.io/rewrite-target" = "/$2"
    }
  }

  depends_on = [
    helm_release.vcluster-control
  ]

  spec {
    ingress_class_name = "nginx"
    rule {
      # FIXME: remove this hardcoded domain and get it from host cluster?
      host = "whome.sneak.codes"
      http {
        path {
          path = "/${local.release_name}-control(/|$)(.*)"
          backend {
            service {
              name = "ingress-nginx-control-controller"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}



# code-server
data "coder_workspace" "me" {}

resource "coder_agent" "main" {
  os             = "linux"
  arch           = "amd64"
  startup_script = <<EOT
    #!/bin/bash
    if [ ! -f ~/.profile ]; then
      cp /etc/skel/.profile $HOME
    fi
    if [ ! -f ~/.bashrc ]; then
      cp /etc/skel/.bashrc $HOME
    fi

    code-server --auth none --port 13337 | tee code-server-install.log &
  EOT
}

resource "coder_app" "code-server" {
  agent_id      = coder_agent.main.id
  display_name  = "code-server"
  slug          = "code-server"
  icon          = "/icon/code.svg"
  url           = "http://localhost:13337?folder=/home/coder"
  subdomain     = false
  share         = "owner"

  healthcheck {
    url       = "http://localhost:13337/healthz"
    interval  = 3
    threshold = 10
  }
}

resource "kubernetes_pod" "main" {
  count = data.coder_workspace.me.start_count
  metadata {
    name      = "coder-${lower(data.coder_workspace.me.owner)}-${lower(data.coder_workspace.me.name)}"
    namespace = local.release_namespace
  }

  depends_on = [
    helm_release.vcluster-dev,
    helm_release.vcluster-prod,
    helm_release.vcluster-control
  ]

  spec {
    security_context {
      run_as_user = "1000"
      fs_group    = "1000"
    }
    container {
      name    = "dev"
      image   = "registry.gitlab.com/datach17d/infra/coder-templates/kubernetes-vcluster:v4.22.1-1"
      command = ["sh", "-c", coder_agent.main.init_script]
      security_context {
        run_as_user = "1000"
      }
      env {
        name  = "CODER_AGENT_TOKEN"
        value = coder_agent.main.token
      }
      volume_mount {
        mount_path = "/home/coder"
        name       = "home"
        read_only  = false
      }
      env {
        name  = "KUBECONFIG"
        value = "/tmp/control/config:/tmp/dev/config:/tmp/prod/config"
      }
      volume_mount {
        mount_path = "/tmp/dev"
        name       = "vcluster-config-dev"
        read_only  = true
      }
      volume_mount {
        mount_path = "/tmp/prod"
        name       = "vcluster-config-prod"
        read_only  = true
      }
      volume_mount {
        mount_path = "/tmp/control"
        name       = "vcluster-config-control"
        read_only  = true
      }
    }

    volume {
      name = "home"
      empty_dir {
      }
    }
    volume {
      name = "vcluster-config-dev"
      secret {
        secret_name = "vc-${local.release_name}-dev"
      }
    }
    volume {
      name = "vcluster-config-prod"
      secret {
        secret_name = "vc-${local.release_name}-prod"
      }
    }
    volume {
      name = "vcluster-config-control"
      secret {
        secret_name = "vc-${local.release_name}-control"
      }
    }
  }
}
